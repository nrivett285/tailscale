FROM rancher/kubectl:v1.23.7 as kubectl
#Stealing this container as it has kubectl for all arches
#https://stackoverflow.com/questions/51717471/how-to-install-kubectl-in-kubernetes-container-through-docker-image

FROM tailscale/tailscale:v1.36
# Base is Alpine
#ARG user=nicknack
#ARG home=/home/$user
#RUN addgroup -S docker
#RUN adduser -D --ingroup docker --home=/home/nicknack nicknack
#RUN cat /etc/os-release
#RUN echo 'nicknack:gallantly-84523-composed' | chpasswd
#RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 test 
#RUN  echo 'test:test' | chpasswd
RUN apk update
RUN apk add openrc openssh
COPY --from=kubectl /bin/kubectl /usr/local/bin/


RUN rc-update add sshd
ADD https://raw.githubusercontent.com/tailscale/tailscale/54e8fa1/docs/k8s/run.sh /init
RUN ["chmod", "+x", "/init"]
EXPOSE 22
CMD ["init"]